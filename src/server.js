const express = require('express');
const app = express();
const path = require('path');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require ('connect-flash');
const morgan = require ('morgan');
const cookieParser = require ('cookie-parser');
const bodyParser = require('body-parser');
const session = require ('express-session');

//var requestjson = require('request-json');
//var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca4mb79734/collections/movimientos?apiKey=t8ghbjSKSMlr9zKDGamhJsHAYLUX9H-S";
//var clienteMlab = requestjson.createClient(urlmovimientosMlab);
const { mongodbUri } = require ('./config/database');
const { usuario } = require ('./config/database');
const { contrasena } = require ('./config/database');

mongoose.connect(mongodbUri, {
  useNewUrlParser: true,
  //useMongoClient = true,
  auth: {
    user: usuario,
    password: contrasena
  }
})
var conn = mongoose.connection;
conn.on('error', console.error.bind(console, 'connection error:'));

conn.once('open', () =>{
 console.log('connected to adatabase OK')
});

require('./config/passport')(passport);

//settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname,'views'));
app.set('view engine', 'ejs');
//middleware
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(function(req,res,next){
 res.setHeader("Access-Control-Allow-Origin","*");
 //res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
 res.setHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Authorization");
 res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
 res.setHeader('Access-Control-Allow-Credentials', true);
 next();
});


app.use(session({
  secret :'ifdhsliufhdsqwqxvfhdsjkfldsfd',
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//routers
//require('./app/routes')(app.passport);

//static files
const routes = require('./app/routes');
app.get('/', function(req, res) {
  res.send('Page under construction.');
});
app.use(routes);
app.use(express.static(path.join(__dirname,'public')));

app.listen(app.get('port'), () => {
  console.log('server on port', app.get('port'));
});

module.exports = app;
