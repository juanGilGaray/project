const mongoose = require('mongoose');

const tarjetaSchema = new mongoose.Schema({
    userId: String,
    numeroTarjeta: String,
    banco: String,
    fechaCaducidad: String

});

module.exports = mongoose.model('Tarjeta', tarjetaSchema);
