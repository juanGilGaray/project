const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const monivientosSchema = new mongoose.Schema({
    tarjetaId: String,
    monto: String,
    motivo: String,
    fecha: String
});


module.exports = mongoose.model('Movimientos', monivientosSchema);
