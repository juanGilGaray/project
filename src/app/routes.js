var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
const express = require('express');
const app = express();
var jwt = require('jsonwebtoken');
const { secret } = require ('../config/database');

const User = require('../app/models/user');
const Tarjeta = require('../app/models/tarjeta');
const Movimientos = require('../app/models/movimientos');

app.get('/', function(req, res, next) {
  console.log("prueba");
  res.send({ "title": "Express" });
});

app.post('/login', function(req, res) {
  User.findOne({
    'email': req.body.email
  }, function(err, user) {
    if (err) throw err;
    if (!user) {
      res.status(401).send({success: false, msg: 'usuaurio no encontrado.'});
    } else {
      if(!user.validPassword(req.body.password)){
        res.status(401).send({success: false, msg: 'contraseña incorrecta.'});
      }else{
        const opts = {};
        opts.expiresIn = 12000;
        var token = jwt.sign({"id":user._id}, secret, opts);
        res.json({success: true,"nombre":user.nombre,"apellido":user.apellido, token: 'JWT ' + token});
      };
    }
  });
});

app.post('/signup', function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.json({success: false, msg: 'Por favor ingrese su email y contraseña.'});
  } else {
    User.findOne({
      'email': req.body.email
    }, function(err, user) {
      if (err) throw err;
      if (user) {
        return res.status(401).send({success: false, msg: 'el usuario ya existe.'});
      }else{
        var newUser = new User();
        newUser.email = req.body.email;
        newUser.nombre = req.body.nombre;
        newUser.password = newUser.generateHash(req.body.password);
        // save the user
        newUser.save(function(err) {
          if (err) {
            return res.status(401).send({success: false, msg: 'ocurrio un error.'});
          }
          res.json({success: true, msg: 'el usuario se guardo con exito.'});
        });
      }
    });
  }
});


//**********************************Tarjetas **********************************
app.post('/tarjeta', passport.authenticate('jwt', { session: false}), function(req, res) {
  var validaTarjetas = funvalidaTarjetas(req);
  if (validaTarjetas) {
    Tarjeta.findOne({
      'numeroTarjeta': req.body.numeroTarjeta
    }, function(err, tarjeta) {
      if (err) throw err;
      if (tarjeta && req.user._id == tarjeta.userId) {
        return res.status(401).send({success: false, msg: 'La tarjeta ya se encuentra registrada.'});
      }
    else{
      var newTarjeta = new Tarjeta({
        userId:req.user._id,
        numeroTarjeta: req.body.numeroTarjeta,
        banco: req.body.banco,
        fechaCaducidad: req.body.fechaCaducidad
      });
      newTarjeta.save(function(err) {
        if (err) {
          return res.status(401).send({success: false, msg: 'Error al guardar la tarjeta'});
        }
        res.json({success: true, msg: 'Tarjeta guardada exitosamente'});
      });
    }});
    } else {
      return res.status(403).send({success: false, msg: 'Datos no validos'});
    }
});

app.get('/tarjeta/:numeroTarjeta', passport.authenticate('jwt', { session: false}), function(req, res) {
  //console.log("valida pruebas: ",req.params.numeroTarjeta);
  var token = getToken(req.headers);
  if (Number(req.params.numeroTarjeta)) {
      Tarjeta.findOne({
        'numeroTarjeta': req.params.numeroTarjeta
      }, function(err, tarjeta) {
        if (err) throw err;
        if (tarjeta && req.user._id == tarjeta.userId) {
          res.json({success: true, "numeroTarjeta": tarjeta.numeroTarjeta, "banco": tarjeta.banco, "fechaCaducidad":tarjeta.fechaCaducidad});
        }
      else{
          return res.status(401).send({success: false, msg: 'Tarjeta no registrada'});
      }
    });
  }else{
    return res.status(403).send({success: false, msg: 'Dato no valido.'});
  }
});

app.get('/tarjeta', passport.authenticate('jwt', { session: false}), function(req, res) {
  //console.log("valida pruebas: ",req.params.numeroTarjeta);
  var token = getToken(req.headers);
  if (token) {
      Tarjeta.find({
        'userId': req.user._id
      }, function(err, tarjeta) {
        if (err) throw err;
        if (tarjeta) {
          res.json({success: true, data: tarjeta});
        }
      else{
          return res.status(401).send({success: false, msg: 'Tarjeta no registrada'});
      }
    });
  }else{
    return res.status(403).send({success: false, msg: 'No autorizado.'});
  }
});

app.delete('/tarjeta/:numeroTarjeta', passport.authenticate('jwt', { session: false}), function(req, res) {
  //console.log("valida pruebas: ",req);
  if (Number(req.params.numeroTarjeta)) {
      Tarjeta.findOneAndDelete({
        'numeroTarjeta': req.params.numeroTarjeta
      }, function(err, tarjeta) {
        if (err) throw err;
        if (tarjeta && req.user._id == tarjeta.userId) {
          res.json({success: true,  msg: 'Tarjeta eliminada'});
        }
      else{
          return res.status(401).send({success: false, msg: 'Tarjeta no registrada'});
      }
    });
  }else{
    return res.status(403).send({success: false, msg: 'Dato no valido'});
  }
});


app.patch('/tarjeta/:numeroTarjeta', passport.authenticate('jwt', { session: false}), function(req, res) {
  //console.log("valida pruebas: ",req.params.numeroTarjeta);
  var validaTarjetas = funvalidaTarjetas(req);
  if (Number(req.params.numeroTarjeta) && validaTarjetas) {
      Tarjeta.findOneAndUpdate({
        'numeroTarjeta': req.params.numeroTarjeta
      },
      {$set:
        { 'numeroTarjeta':req.body.numeroTarjeta,
          'banco':req.body.banco,
          'fechaCaducidad':req.body.fechaCaducidad}
      },
      function(err, doc) {
        if(err){
          return res.status(401).send({success: false, msg: 'Error en la operacion'});
        } else if(!doc){
          return res.status(401).send({success: false, msg: 'No se encontro la tarjeta'});
        }else{
          res.json({success: true, msg: 'Actualizacion correcta'});
        }
    });
}else{
  return res.status(401).send({success: false, msg: 'Dato no valido'});
}});

//**********************************movimientos **********************************

app.post('/movimientos',passport.authenticate('jwt', { session: false}), function(req, res) {

  var validaMovimientos = funvalidaMovimientos(req);
  if (validaMovimientos) {
      Movimientos.findOne(
    {'tarjetaId': req.body.tarjetaId,
    'motivo': req.body.motivo},
    function(err, movimientos) {
      if (err) throw err;
      //console.log("movimientos: ",movimientos);
      //console.log("tarjeta: ",req.body.tarjetaId)
      if (movimientos ) {
        return res.status(401).send({success: false, msg: 'la operacion ya se encuentra registada'});
      }
    else{
      var newMovimientos = new Movimientos({
        tarjetaId: req.body.tarjetaId,
        monto: req.body.monto,
        motivo: req.body.motivo,
        fecha: req.body.fecha
      });
      newMovimientos.save(function(err) {
        if (err) {
          return res.status(401).send({success: false, msg: 'Error al guardar la operacion'});
        }
        res.json({success: true, msg: 'operacion guardada correctamente'});
      });
    }});
    } else {
      return res.status(403).send({success: false, msg: 'Datos no validos.'});
    }
});


app.get('/movimientos/:tarjetaId', passport.authenticate('jwt', { session: false}),function(req, res) {
  //console.log("valida pruebas: ",req.params.numeroTarjeta);
  //var token = getToken(req.headers);
  if (Number(req.params.tarjetaId)) {
      Movimientos.find({
        'tarjetaId': req.params.tarjetaId
      }, function(err, movimientos) {
        if (err) throw err;
        if (movimientos) {
          res.json({success: true, data: movimientos});
        }
      else{
          return res.status(401).send({success: false, msg: 'No existen movimientos para esta tarjeta'});
      }
    });
  }else{
    return res.status(403).send({success: false, msg: 'Dato no valido.'});
  }
});

app.delete('/movimientos/:motivo', passport.authenticate('jwt', { session: false}), function(req, res) {
  //console.log("valida pruebas: ",req);
  var token = getToken(req.headers);
  if (token) {
      Movimientos.findOneAndDelete(
        {'tarjetaId': req.body.tarjetaId,
        'motivo': req.params.motivo},
        function(err, movimientos) {
        if (err) throw err;
        if (movimientos) {
          res.json({success: true,  msg: 'Operacion eliminada'});
        }
      else{
          return res.status(401).send({success: false, msg: 'La operaicon no existe'});
      }
    });
  }else{
    return res.status(403).send({success: false, msg: 'No autorizado.'});
  }
});

app.patch('/movimientos/:motivo', passport.authenticate('jwt', { session: false}), function(req, res) {
  /*console.log("valida pruebas: ",req.params.numeroTarjeta);
  console.log("tarjetaId: ",req.body.tarjetaId);
  console.log("monto: ",req.body.monto);
  console.log("motivo: ",req.body.motivo);
  console.log("fecha: ",req.body.fecha);
  console.log("motivo parametro: ",req.params.motivo);*/
  var validaMovimientos = funvalidaMovimientos(req);
  if (validaMovimientos) {
      Movimientos.findOneAndUpdate(
        {'tarjetaId': req.body.tarjetaId,
        'motivo': req.params.motivo},
      {$set:
      { 'tarjetaId':req.body.tarjetaId,
        'monto':req.body.monto,
        'motivo':req.body.motivo,
        'fecha':req.body.fecha}
      },
      function(err, doc) {
        if(err){
          return res.status(401).send({success: false, msg: 'Error en la operacion'});
        } else if(!doc){
          return res.status(401).send({success: false, msg: 'No se encontrol la operacion'});
        }else{
          res.json({success: true, msg: 'Operacion correcta'});
        }
    });
}else{
    return res.status(401).send({success: false, msg: 'datos no validos'});
}});


getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

funvalidaMovimientos = function (req) {
  //console.log("numero: ",Number(req.body.tarjetaId));
  //console.log("String: ",Number(req.body.tarjetaId));
  if(req.body.tarjetaId.length > 15){
    if(Number(req.body.tarjetaId) == req.body.tarjetaId){
      //console.log('tarjetaOK');
      if(Number(req.body.monto) == req.body.monto){
        //console.log('montoOK');
        let partes = (req.body.fecha || '').split('-'),
        fechaGenerada = new Date(partes[0], partes[1], partes[2]);
        //console.log("fecha:",fechaGenerada);
        if (partes.length == 3 && fechaGenerada
           && partes[2] == fechaGenerada.getDate()
           && partes[1] == fechaGenerada.getMonth()
           && partes[0] == fechaGenerada.getFullYear()) {
              return 'valida ok';
        }else{
          return null;
        }
      }
    }
  }

};

funvalidaTarjetas = function (req) {
  /*console.log("banco: ",req.body.banco);
  console.log("banco length: ",req.body.banco.length);
  console.log("numeroTarjeta: ",req.body.numeroTarjeta);
  console.log("numeroTarjeta: ",req.body.numeroTarjeta.length);
  console.log("numero: ",Number(req.body.numeroTarjeta));
  if((Number(req.body.numeroTarjeta)) == req.body.numeroTarjeta){
    console.log("Es un numero");
  }else{
    console.log("No es un numero");
  }*/

  if(req.body.banco.length > 0){
    //console.log("Banco OK");
    if(req.body.numeroTarjeta.length > 15){
      //console.log("tarjeta length OK");
      if(Number(req.body.numeroTarjeta) == req.body.numeroTarjeta){
        //  console.log("tarjeta es un numero");
        //  console.log("tarjeta: ",req.body.fechaCaducidad);
          let partes = (req.body.fechaCaducidad || '').split('-'),
          fechaGenerada = new Date(partes[0], partes[1], partes[2]);
        /*  console.log("fechaGenerada: ",fechaGenerada);
          console.log("getDate: ",fechaGenerada.getDate(), " ",partes[2]);
          console.log("getMonth: ",fechaGenerada.getMonth(), " ",partes[1]);
          console.log("getFullYear: ",fechaGenerada.getFullYear(), " ",partes[0]);*/

          if (partes.length == 3 && fechaGenerada
             && partes[2] == fechaGenerada.getDate()
             && partes[1] == fechaGenerada.getMonth()
             && partes[0] == fechaGenerada.getFullYear()) {
                return 'valida ok';
          }else{
            return null;
          }
      }
    }
  }

};

module.exports = app;
