const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const { secret } = require ('../config/database');
// load up the user model
const User = require('../app/models/user');

module.exports = function(passport) {
  var opts = {};
  //fromAuthHeaderAsBearerToken
  //console.log("valida");
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
  opts.secretOrKey = secret;
  passport.use('jwt',new JwtStrategy(opts, function(jwt_payload, done) {
    User.findOne({_id: jwt_payload.id}, function(err, user) {
          if (err) {
            return done(err, false);
            //console.log("erorr de op");
          }
          if (user) {
            //console.log("usuario valido: ",user);
             return done(null, user);
          } else {
            //console.log("usuario no encontrado");
            done(null, false);
          }
      });
  }));
};
